package com.example.mob202_ps10222_lab03;

import android.os.Environment;
import java.io.File;
import java.util.ArrayList;

public class ImagesManager {
    // SDCard Path
    final String ALBUM_PATH = Environment.getExternalStorageDirectory()
            .getPath() + "/";
    private ArrayList<String> imgsList= new ArrayList<String>();
    private String jpgPattern = ".jpg";
//    private String pngPattern = ".png";

    // Constructor
    public ImagesManager() {
    }

    /**
     * Function to read all files and store the details in
     * ArrayList
     * */
    public ArrayList<String> getPlayList() {
        if (ALBUM_PATH != null) {
            File home = new File(ALBUM_PATH);
            File[] listFiles = home.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    System.out.println(file.getAbsolutePath());
                    if (file.isDirectory()) {
                        scanDirectory(file);
                    } else {
                        addImgToList(file);
                    }
                }
            }
        }
        // return imgs list array
        return imgsList;
    }

    private void scanDirectory(File directory) {
        if (directory != null) {
            File[] listFiles = directory.listFiles();
            if (listFiles != null && listFiles.length > 0) {
                for (File file : listFiles) {
                    if (file.isDirectory()) {
                        scanDirectory(file);
                    } else {
                        addImgToList(file);
                    }
                }
            }
        }
    }

    private void addImgToList(File song) {
        //adding JPEG format Images
        if (song.getName().endsWith(jpgPattern)) {
            imgsList.add(song.getPath());
        }
    }
}
