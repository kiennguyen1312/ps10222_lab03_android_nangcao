package com.example.mob202_ps10222_lab03;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class PS10222_LAB3_CAU3 extends AppCompatActivity {

    ImageAdapter myImageAdapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab3__cau3);

        // Ánh xạ
        GridView gridview = (GridView) findViewById(R.id.gv_album);
        myImageAdapter = new ImageAdapter(this);
        gridview.setAdapter(myImageAdapter);

        ImagesManager im = new ImagesManager();
        ArrayList<String> images=im.getPlayList();

        for (String image:images){
            myImageAdapter.add(image);
        }
    }
}