package com.example.mob202_ps10222_lab03;

import android.Manifest;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.CallLog;
import android.provider.CallLog.Calls;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class PS10222_LAB3_CAU2 extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG = 9;
    TextView tvShowCallLog;

    @Override
        protected void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_ps10222__lab3__cau2);
            tvShowCallLog = findViewById(R.id.tvShowCallLog);

            requestPermission();
            accessTheCallLog();
        }

        /**
         * hàm lấy danh sách lịch sử cuộc gọi
         * với thời gian nhỏ hơn 30 giây và sắp xếp theo ngày gọi
         */
        public void accessTheCallLog() {
            String[] projection = new String[]{
                    Calls.DATE,
                    Calls.NUMBER,
                    Calls.DURATION
            };
            Cursor c = getContentResolver().query(
                    CallLog.Calls.CONTENT_URI,
                    projection,
                    Calls.DURATION + "<?", new String[]{"30"},
                    Calls.DATE + " Asc");
            c.moveToFirst();
            String s = "";
            while (c.isAfterLast() == false) {
                for (int i = 0; i < c.getColumnCount(); i++) {
                    s += c.getString(i) + " - ";
                }
                s += "\n";
                c.moveToNext();
            }
            c.close();
            Toast.makeText(this, s, Toast.LENGTH_LONG).show();

            tvShowCallLog.setText(s);
        }

    // Xin quyền:
    public void requestPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CALL_LOG)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CALL_LOG)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CALL_LOG},
                        MY_PERMISSIONS_REQUEST_READ_CALL_LOG);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }

}


