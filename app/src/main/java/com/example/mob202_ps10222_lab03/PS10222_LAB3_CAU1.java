package com.example.mob202_ps10222_lab03;

import android.Manifest;
import android.content.ContentResolver;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class PS10222_LAB3_CAU1 extends AppCompatActivity {
    private static final int MY_PERMISSIONS_REQUEST_READ_CALL_LOG = 9;
    ListView lv_hien_thi_danhba;
    ArrayList<String> ds = new ArrayList<String>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ps10222__lab3__cau1);
        // Ánh xạ:
        lv_hien_thi_danhba = findViewById(R.id.lv_hien_thi_danhba);

        requestPermission();
        readAllContacts();
    }

    // Đọc danh bạ:
    public void readAllContacts(){
        ContentResolver ctr = getContentResolver();
        Cursor c = ctr.query(ContactsContract.Contacts.CONTENT_URI,
                null,null,null,null);
        if (c.moveToFirst()){
            do {
                String chuoi="";

                String id = c.getString(c.getColumnIndex(ContactsContract.Contacts._ID));
                String name = c.getString(c.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
                chuoi+=name;

                int hasphone = Integer.parseInt(c.getString(c.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER)));
                if (hasphone>0){
                    Cursor cphone = ctr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null, //Các cột cần lấy = null
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?", //Menh de Where
                            new String[]{id},   // Đối số mệnh đề Where
                            null // sort
                    );
                    if (cphone.moveToFirst()){
                        do {
                            String num = cphone.getString(cphone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                            chuoi+="\n" + num;
                        }while (cphone.moveToNext());
                    }
                }
                ds.add(chuoi);
            } while (c.moveToNext());
        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1,ds);
        lv_hien_thi_danhba.setAdapter(adapter);
        Toast.makeText(this, "Size: " + ds.size(), Toast.LENGTH_SHORT).show();
        c.close();
    }

    // Xin quyền:
    public void requestPermission() {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.READ_CONTACTS)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.READ_CONTACTS)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.READ_CONTACTS},
                        MY_PERMISSIONS_REQUEST_READ_CALL_LOG);
                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {
            // Permission has already been granted
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CALL_LOG: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}